
public class Sheep {

   enum Animal {sheep, goat}

   public static void main (String[] param) {
      // for debugging
   }

   public static void reorder (Animal[] animals) {

      Animal[] reordered = new Animal[animals.length];
      int counter = 0;
      int backwardsPointer = animals.length -1;

      for (Animal animal : animals) {
         if (animal == Animal.goat){
            reordered[counter] = animal;
            counter++;
         } else {
            reordered[backwardsPointer] = animal;
            backwardsPointer--;
         }
      }
      System.arraycopy(reordered, 0, animals, 0, animals.length);

   }
}